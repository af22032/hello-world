import java.util.Scanner;

public class practice1 {
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        System.out.println("What would you like to order: ");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.print("Your order: ");
        int order = userIn.nextInt();
        switch (order) {
            case 1:
                System.out.println("You have ordered Tempura. Thank you!");
                break;
            case 2:
                System.out.println("You have ordered Ramen. Thank you!");
                break;
            case 3:
                System.out.println("You have ordered Udon. Thank you!");
                break;
            default:
        }
    }
}
