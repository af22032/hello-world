import java.util.Scanner;

public class practice2 {
    static void order(String food){
        System.out.println("You have ordered " + food + ". Thank you!");
    }
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        System.out.println("What would you like to order: ");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.print("Your order: ");
        int order = userIn.nextInt();
        switch (order) {
            case 1:
                order("Tempura");
                break;
            case 2:
                order("Ramen");
                break;
            case 3:
                order("Udon");
                break;
            default:
        }
    }
}
