import java.util.Scanner;

public class practicex {
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        System.out.println("What would you like to order: ");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.print("Your order: ");
        int order = userIn.nextInt();
        if(order >= 1 && order <= 3) {
            System.out.print("You have ordered ");
            switch (order) {
                case 1:
                    System.out.print("Tempura");
                    break;
                case 2:
                    System.out.print("Ramen");
                    break;
                default:
                    System.out.print("Udon");
            }
            System.out.println(". Thank you!");
        }
    }
}
